<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ifind');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0g]~]tGVS;>799.f=pbb#Ko-:p(8w!2JEuw}_?my_`5gk?T$AbO.)|. fj:-ZP*7');
define('SECURE_AUTH_KEY',  'MTOu;idulTph>|]~m%NOkhgWS4K:Sltb+x$6><A/iteZfVYed`H]H^|<mKGz+9Cv');
define('LOGGED_IN_KEY',    'n+R.Tx6m5;p)hUO0:)=XvxAIHvSc{C ~|[z2Zft}Fcpe)dc/jyVE;&J<SC1,_t!J');
define('NONCE_KEY',        '@!Xv-ogx#T6*e4KdlLkDC~qtOV_zcfjChQ/B*_z4d=.`.Xk3Xhf#V`+jRT>PoeN_');
define('AUTH_SALT',        'G.l:hHCl<rI6;|@[ZfF(ZfTO*+lQ@|!?!W2({ukZ]11w&RoH!}pb]+1:}j[Y}nl[');
define('SECURE_AUTH_SALT', 'NM4.18eK_Sic~6n)MT?zmW>P%7u.o`[BDaIK>9<OkT$B/,p8#5LNm{.?szG2T6xA');
define('LOGGED_IN_SALT',   '-]u/O-&&NS7-JV_;D<4<BI13p-2y:dLh.CsX}* S-Vm,!42Z_XR5W|(QlR$b`A2o');
define('NONCE_SALT',       'TUMbN!EF<Y5[Rta|:hn`w5Ofw(5ET,VNolPkz<z? wgYRJ{{g,G)*bNKsB mXyVp');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/ifind/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
